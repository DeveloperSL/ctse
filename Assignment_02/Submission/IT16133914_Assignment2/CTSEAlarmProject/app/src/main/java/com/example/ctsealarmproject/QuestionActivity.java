package com.example.ctsealarmproject;

import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class QuestionActivity extends AppCompatActivity {

    private RadioGroup radioAnswerGroup;
    private RadioButton radioAnswerButton;
    Context context;
    int alarmID = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);

        //Initialize Buttons
        Button checkAlarm = (Button) findViewById(R.id.btnCheck);
        this.context = this;

        radioAnswerGroup = (RadioGroup) findViewById(R.id.radioAnswer);

        checkAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intentToSetAlarm = new Intent(context,SetAlarm.class);

                if(alarmID != -1){
                    int selectedId = radioAnswerGroup.getCheckedRadioButtonId();

                    // find the radiobutton by returned id
                    radioAnswerButton = (RadioButton) findViewById(selectedId);
                    Log.e("Radio:",radioAnswerButton.getText().toString());
                    if(radioAnswerButton.getText().toString().equals("H2O")){
                        intentToSetAlarm.putExtra("source","QuestionActivity");
                        intentToSetAlarm.putExtra("alarmID",alarmID);
                        intentToSetAlarm.putExtra("correctAnswer", "true");

                        startActivity(intentToSetAlarm);
                    }

                }

            }
        });


    }

    @Override
    protected void onStart() {
        super.onStart();
        alarmID = getIntent().getExtras().getInt("alarmID");

    }

}
