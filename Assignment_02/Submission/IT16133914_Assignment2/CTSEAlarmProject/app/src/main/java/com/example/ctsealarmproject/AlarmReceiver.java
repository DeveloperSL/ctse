package com.example.ctsealarmproject;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e("Receiver Invoked","In Receiver");

        String source = intent.getExtras().getString("source");
        if(source.equals("SetAlarm") || source.equals("UnSetAlarm")){
            Log.e("Receiver Invoked",source);

            //fetch extra strings from intent
            //Tells the app whether the user pressed alarm off or on button
            String getExtraString = intent.getExtras().getString("extra");

            //Modified
            //fetch the alarm id
            Integer getAlarmIDString = intent.getExtras().getInt("alarmID");

            Log.e("Value of the key ",getAlarmIDString.toString());

            //fetch extra int from intent
            //Tells the app which value the user picked from the drop down menu/spinner
            Integer getExtraLongRingtoneChoice = intent.getExtras().getInt("ringtoneChoice");

            Log.e("The ringtone choice is ", getExtraLongRingtoneChoice.toString());


            //Modified

            //Create an intent to the ringtone service
            Intent intentToRingtonePlayingService = new Intent(context,RingtonePlayingService.class);

            //Pass the extra string from receiver to RingtonePlayingService
            intentToRingtonePlayingService.putExtra("extra",getExtraString);
            //Modified
            intentToRingtonePlayingService.putExtra("alarmID",getAlarmIDString);
            //Pass the extra integer from the receiver to RingtonePlayingService
            intentToRingtonePlayingService.putExtra("ringtoneChoice",getExtraLongRingtoneChoice);

            //Start the ringtone service
            context.startService(intentToRingtonePlayingService);
        }
        else if(source.equals("QuestionActivity")){
            Log.e("Receiver Invoked By",source);


        }

    }
}
