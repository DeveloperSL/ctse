package com.example.ctsealarmproject;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import java.util.Random;

import static android.app.Activity.RESULT_OK;


public class RingtonePlayingService extends Service {

    MediaPlayer mediaPlayerSong;
    boolean isRunning;
    int startId;

    Integer alarmId;
    private static final int REQUEST_CODE = 1;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("LocalService", "Received start id " + startId + ": " + intent);

        //fetch the extra string from the alarm on/alarm off values
        String state = intent.getExtras().getString("extra");

        //Modified
        //fetch the alarmId
        alarmId = intent.getExtras().getInt("alarmID");

        //fetch the ringtoneChoice values
        Integer ringtoneSoundChoice = intent.getExtras().getInt("ringtoneChoice");

        Log.e("Ringtone state extra is",state);
        Log.e("Ringtone choice is",ringtoneSoundChoice.toString());



        //converts extra strings from intent to startIds value 0 or 1
        assert  state != null;
        switch (state) {
            case "alarm on":
                startId = 1;
                break;
            case "alarm off":
                startId = 0;
                Log.e("startId is ",String.valueOf(startId));
                break;
            default:
                startId = 0;
                break;
        }


        //if else stack

        //if there is no music playing, and user pressed the "alarm on"
        //music should start playing
        if(!this.isRunning && startId == 1){

            Log.e("there is no music and","you want start");

            this.isRunning = true;
            this.startId = 0;

            /*// Notification is here

            //Notification
            //set up notification  service
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

            //Set up an intent that goes to the SetAlarm activity
            Intent intentToSetAlarmActivity = new Intent(this.getApplicationContext(),SetAlarm.class);

            //Set up the pending intent
            PendingIntent pendingIntentToSetAlarmActivity = PendingIntent.getActivity(this,0,intentToSetAlarmActivity,0);


            //Make the notification parameters
            Notification notificationPopup = new Notification.Builder(this).setContentTitle("Your Alarm is going off!!")
                    .setContentText("Click Here!")
                    .setContentIntent(pendingIntentToSetAlarmActivity)
                    .setAutoCancel(true)
                    .build();

            //Set up the notification call command
            notificationManager.notify(0,notificationPopup);
*/

            //Play the ringtone sound depending on the passed ringtoneChoice id
            if(ringtoneSoundChoice == 0){
                //play a randomly picked audio file

                int minNumber = 1;
                int maxNumber = 5;

                Random randomNumber = new Random();
                int ringtoneNumber = randomNumber.nextInt(maxNumber + minNumber);
                Log.e("Random number is",String.valueOf(ringtoneNumber));

                if(ringtoneNumber == 1){
                    //Create an instance of media player
                    mediaPlayerSong = MediaPlayer.create(this,R.raw.beautiful_christmas_tune);
                    //Start the ringtone
                    mediaPlayerSong.start();
                }
                else if(ringtoneNumber == 2){
                    mediaPlayerSong = MediaPlayer.create(this,R.raw.dreamy_christmas_bells);
                    mediaPlayerSong.start();
                }
                else if(ringtoneNumber == 3){
                    mediaPlayerSong = MediaPlayer.create(this,R.raw.goodmorning);
                    mediaPlayerSong.start();
                }
                else if(ringtoneNumber == 4){
                    mediaPlayerSong = MediaPlayer.create(this,R.raw.soft_bells);
                    mediaPlayerSong.start();
                }
                else if(ringtoneNumber == 5){
                    mediaPlayerSong = MediaPlayer.create(this,R.raw.wake_up_will_you);
                    mediaPlayerSong.start();
                }
                else {
                    mediaPlayerSong = MediaPlayer.create(this,R.raw.goodmorning);
                    mediaPlayerSong.start();
                }

            }
            else if(ringtoneSoundChoice == 1){
                //Create an instance of media player
                mediaPlayerSong = MediaPlayer.create(this,R.raw.beautiful_christmas_tune);
                //Start the ringtone
                //Modified
                    mediaPlayerSong.setLooping(true);
                    mediaPlayerSong.start();
                    popUpQuestions();

            }
            else if(ringtoneSoundChoice == 2){

                mediaPlayerSong = MediaPlayer.create(this,R.raw.dreamy_christmas_bells);
                mediaPlayerSong.setLooping(true);
                mediaPlayerSong.start();
                popUpQuestions();
            }
            else if(ringtoneSoundChoice == 3){

                mediaPlayerSong = MediaPlayer.create(this,R.raw.goodmorning);
                mediaPlayerSong.setLooping(true);
                mediaPlayerSong.start();
                popUpQuestions();
            }
            else if(ringtoneSoundChoice == 4){

                mediaPlayerSong = MediaPlayer.create(this,R.raw.soft_bells);
                mediaPlayerSong.setLooping(true);
                mediaPlayerSong.start();
                popUpQuestions();
            }
            else if(ringtoneSoundChoice == 5){

                mediaPlayerSong = MediaPlayer.create(this,R.raw.wake_up_will_you);
                mediaPlayerSong.setLooping(true);
                mediaPlayerSong.start();
                popUpQuestions();
            }
            else {

                mediaPlayerSong = MediaPlayer.create(this,R.raw.goodmorning);
                mediaPlayerSong.setLooping(true);
                mediaPlayerSong.start();
                popUpQuestions();
            }


        }
        //if there is music playing, and user pressed the "alarm off"
        //music should stop playing
        else if(this.isRunning && startId == 0){
            Log.e("there is  music and","you want end");

            //Stop the ringtone
            mediaPlayerSong.stop();
            mediaPlayerSong.reset();

            this.isRunning = false;
            this.startId = 0;
        }

        //these are if the users press random buttons
        //Just to bug-proof the app

        //if there is no music playing, and user pressed the "alarm off"
        //do nothing
        else if (!this.isRunning && startId == 0){
            Log.e("there is no music and","you want end");

            this.isRunning = false;
            this.startId = 0;

        }
        //if there is  music playing, and user pressed the "alarm on"
        //do nothing
        else if (this.isRunning && startId == 1){
            Log.e("there is music and","you want start");

            this.isRunning = true;
            this.startId = 1;
        }
        //catch the odd event
        else {
            Log.e("else","you reached this");

        }

        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {

        Log.e("on Destroy called","on Destroy");
        super.onDestroy();

        // Tell the user we stopped.
        Toast.makeText(this, "on Destroy called", Toast.LENGTH_SHORT).show();
    }


    public void popUpQuestions() {
        //add the function to perform here
        Intent intentToQuestionActivity = new Intent(this, QuestionActivity.class);
        intentToQuestionActivity.putExtra("alarmID", alarmId);

        this.startActivity(intentToQuestionActivity);
    }

}
